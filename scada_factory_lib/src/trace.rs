/*
 * File: trace.rs
 * Project: trace
 * Created Date: Thursday February 28th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 4:16:22 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::collections::BTreeMap;
use std::ops::{Add, AddAssign};

#[derive(Debug,Clone)]
pub struct Trace {
    peaks: BTreeMap<i64, f64>
}

impl Trace {
    pub fn new() -> Trace {
        Trace { peaks: BTreeMap::new() }
    }

    pub fn from_tree_map(tree_map: BTreeMap<i64, f64>) -> Trace {
        Trace { peaks: tree_map }
    }

    pub fn insert_peak(&mut self, peak: (i64, f64)) {

        let (ptime, pvalue) = peak;
        let previous_value = match self.peaks.remove(&ptime) {
            Some(last_value) => last_value,
            None => 0f64
        };

        self.peaks.insert(ptime, previous_value + pvalue);
    }

    pub fn insert_peaks(&mut self, peaks: &BTreeMap<i64, f64>) {
        for (tpeak, vpeak) in peaks.iter() {
            self.insert_peak((*tpeak, *vpeak));
        }
    }

    pub fn start_time(&self) -> Option<i64> {
        self.peaks.keys().next().and_then(|i|Some(*i))
    }

    pub fn end_time(&self) -> Option<i64> {
        self.peaks.keys().last().and_then(|i|Some(*i))
    }
}


impl Add<Trace> for Trace {
    type Output = Trace;

    fn add(mut self, rhs: Trace) -> Trace {
        self.insert_peaks(&rhs.peaks);
        self
    }
}

impl AddAssign for Trace {
    fn add_assign(&mut self, other: Trace) {
        self.insert_peaks(&other.peaks);
    }
}



#[test]
fn test_trace() {
    let mut trace = Trace::new();

    assert!(trace.start_time() == None);

    trace.insert_peak((1, 1f64));
    trace.insert_peak((2, 2f64));

    let mut trace2 = Trace::new();
    trace2.insert_peak((3, 1.5f64));

    let trace3 = trace + trace2;
    println!("Trace3 is {:?}", trace3);

    assert!(trace3.start_time() == Some(1));
    assert!(trace3.end_time() == Some(3));
}