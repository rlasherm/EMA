/*
 * File: machine_definition.rs
 * Project: factory
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 11:05:57 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use toml::Value;
use failure::{Error, format_err};

use crate::factory::machine::MachineSignals;

#[derive(Debug,Clone)]
pub struct MachineDefinition {
    pub name: String,
    pub signals: MachineSignals,
    pub count: u64
}

impl MachineDefinition {
    pub fn new(val: &Value) -> Result<MachineDefinition, Error> {
        let name = val["name"].as_str().ok_or(format_err!("No name defined for this required product"))?;

        let signals = match val.get("signals") {
            Some(s) => MachineSignals::new(s)?,
            None => { return Err(format_err!("No signals defined for {}", name)); }
        };

        let count = match val.get("count") {
            Some(Value::Integer(i)) => *i as u64,
            _ => { return Err(format_err!("No count defined for {}", name)); }
        };

        Ok(MachineDefinition {
            name: name.to_owned(),
            signals,
            count
        })
    }
}