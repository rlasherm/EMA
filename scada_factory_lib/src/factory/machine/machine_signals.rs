/*
 * File: machine_signals.rs
 * Project: machine
 * Created Date: Tuesday December 11th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th February 2019 11:47:05 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use toml::Value;
use failure::{Error, format_err};

// use rand::prelude::*;
use rand::distributions::Normal;

#[derive(Debug, Clone)]
pub struct MachineSignals {
    pub peak_count: u64,
    pub cycle_duration: f64,
    pub jitter_distribution: Normal,
    pub peak_value_distribution: Normal,
    pub peak_stddev_distribution: Normal
}

impl MachineSignals {

    fn extract_normal(val: &Value) -> Result<Normal, Error> {
        match val {
            Value::Array(ref arr) => {
                if arr.len() != 2 {
                    Err(format_err!("Expecting an array with 2 elements to describe a normal distribution [mean, stddev]."))
                }
                else {
                    let mean = match arr[0] {
                        Value::Float(f) => f,
                        _ => {return Err(format_err!("Normal mean must be a f64."))}
                    };

                    let stddev = match arr[1] {
                        Value::Float(f) => f,
                        _ => {return Err(format_err!("Normal stddev must be a f64."))}
                    };

                    Ok(Normal::new(mean, stddev))
                }
            },
            _ => Err(format_err!("Value must be an array."))
        }
    }

    pub fn new(val: &Value) -> Result<MachineSignals, Error> {
        let peak_count: u64 = match val.get("peak_count").ok_or(format_err!("No peak_count in this signals data structure."))? {
            Value::Integer(i) => *i as u64,
            v => { return Err(format_err!("Cannot cast {:?} into u64 for peak_count", v)) ;}
        };

        let cycle_duration: f64 = match val.get("cycle_duration").ok_or(format_err!("No cycle_duration in this signals data structure."))? {
            Value::Float(f) => *f as f64,
            v => { return Err(format_err!("Cannot cast {:?} into f64 for cycle_duration", v)) ;}
        };

        let jitter_distribution = MachineSignals::extract_normal(val.get("jitter")
                                                                    .ok_or(format_err!("No jitter in this signals data structure."))?
                                    )?;

        let peak_value_distribution = MachineSignals::extract_normal(val.get("peak_value_distribution")
                                                                    .ok_or(format_err!("No peak_value_distribution in this signals data structure."))?
                                    )?;

        let peak_stddev_distribution = MachineSignals::extract_normal(val.get("peak_stddev_distribution")
                                                                    .ok_or(format_err!("No peak_stddev_distribution in this signals data structure."))?
                                    )?;

        Ok(MachineSignals {
            peak_count,
            cycle_duration,
            jitter_distribution,
            peak_value_distribution,
            peak_stddev_distribution
        })
    }
}