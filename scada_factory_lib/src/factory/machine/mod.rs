/*
 * File: mod.rs
 * Project: machine
 * Created Date: Monday December 10th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 11th December 2018 2:29:08 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

pub mod machine_definition;
pub mod machine_signals;

pub use self::machine_definition::MachineDefinition;
pub use self::machine_signals::MachineSignals;