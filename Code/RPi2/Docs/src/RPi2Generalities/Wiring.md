<!--
@Author: ronan
@Date:   31-08-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 09-09-2016
@License: GPL
-->



# Wiring

The RPi 2 pinout is descrided [here](http://pinout.xyz).

![Visually](./Figures/pinout.jpg)

## Uart

You have to use a UART to USB module, for example the [FTDI Friend](https://www.adafruit.com/product/284).
The connection is as follow:

|FTDI port|RPi2 pin|RPi2 GPIO|
|---|---|---|
|GND|PIN 6| |
|RX|PIN 8|GPIO 14|
|TX|PIN 10|GPIO 15|

GPIO 14 and 15 must be configured as ALT5 functions.
The default UART is 8N1 at 115200 bauds.

## JTAG
