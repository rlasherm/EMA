<!--
@Author: ronan
@Date:   31-08-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 09-09-2016
@License: GPL
-->



# Summary

- [Introduction](./Introduction.md)
- [Generalities](./RPi2Generalities/Generalities.md)
    - [Wiring](./RPi2Generalities/Wiring.md)
    - [Addresses](./RPi2Generalities/Addresses.md)
