/**
* @Author: ronan
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-09-2016
* @License: GPL
*/



void __attribute__((interrupt("UNDEF"))) undefined_instuction_handler(void)
{
  error_code_LED(5);
}

void __attribute__((interrupt("SWI"))) software_interrupt_handler(void)
{
  error_code_LED(6);
}

void __attribute__((interrupt("ABORT"))) prefetch_abort_handler(void)
{
  error_code_LED(7);
}

void __attribute__((interrupt("ABORT"))) data_abort_handler(void)
{
  error_code_LED(8);
}

void __attribute__((interrupt("IRQ"))) interrupt_handler(void)
{
  error_code_LED(9);
}

void __attribute__((interrupt("FIQ"))) fast_interrupt_handler(void)
{
  error_code_LED(10);
}
