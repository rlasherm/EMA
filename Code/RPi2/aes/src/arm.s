; @Author: ronan, kevin
; @Date:   31-08-2016
; @Email:  ronan.lashermes@inria.fr
; @Last modified by:   ronan
; @Last modified time: 16-11-2016
; @License: GPL

.section ".text.arm"

.globl put32
put32:
    str r1,[r0]
    bx lr

.globl get32
get32:
    ldr r0,[r0]
    bx lr

.globl getSP
getSP:
    str     sp, [sp]
    ldr r0,[sp]
    bx lr

.globl getPC
getPC:
    ldr r0,[pc]
    bx lr

.globl getCPSR
getCPSR:
    MRS   r0,CPSR
    bx lr

.globl getACTLR
getACTLR:
    MRC p15, 0, r0, c1, c0, 1
    bx  lr

 .globl start_l1cache
 start_l1cache:
     mov r0, #0
     mcr p15, 0, r0, c7, c1, 0 ;@ invalidate caches
     mcr p15, 0, r0, c8, c7, 0 ;@ invalidate tlb
     mrc p15, 0, r0, c1, c0, 0
     orr r0,r0,#0x1000 ;@ instruction
     orr r0,r0,#0x0800 ;@ branch prediction
     ## orr r0,r0,#0x0004 ;@ data
     mcr p15, 0, r0, c1, c0, 0
     bx lr

 .globl stop_l1cache
 stop_l1cache:
     mrc p15, 0, r0, c1, c0, 0
     bic r0,r0,#0x1000 ;@ instruction
     bic r0,r0,#0x0800 ;@ branch prediction
     ## bic r0,r0,#0x0004 ;@ data
     mcr p15, 0, r0, c1, c0, 0
     bx lr

.globl start_vfp
start_vfp:
    @ r1 = Access Control Register
    MRC p15, #0, r1, c1, c0, #2
     @enable full access for p10,11
    ORR r1, r1, #(0xf << 20)
     @access Control Register = r1
    MCR p15, #0, r1, c1, c0, #2
    MOV r1, #0
    @flush prefetch buffer because of FMXR below
    MCR p15, #0, r1, c7, c5, #4
    @and CP 10 & 11 were only just enabled
     @Enable VFP itself
    MOV r0,#0x40000000
     @FPEXC = r0
    FMXR FPEXC, r0

 .globl read_sctlr
 read_sctlr:
     mrc p15,0,r0,c1,c0,0
     bx lr

 .globl invalidate_tlbs
 invalidate_tlbs:
     mov r2,#0
     mcr p15,0,r2,c8,c7,0  ;@ invalidate tlb
     mcr p15,0,r2,c7,c10,4 ;@ Data Synchronuzation Barrier
     bx lr

.globl dummy
dummy:
     bx lr

.globl getMPIDR
getMPIDR:
    mrc p15,0,r0,c0,c0,5 ;@ MPIDR
    bx lr

.globl core_wrapper
core_wrapper:
    mrc p15,0,r0,c0,c0,5 ;@ MPIDR
    mov r1, #0xFF
    ands r1, r1, r0                  @ Core0
    bne wrapper
    ldr  sp, =_stack_top
    b kernel_preinit

wrapper:
    ldr r2, =_stack_top
    cmp r1,#1                        @ Core1
    beq core_one
    cmp r1,#2                        @ Core2
    beq core_two
    cmp r1,#3                        @ Core3
    beq core_three
    b .

other_core_loop:
    movw r0, #16960
    movt r0, #15
    bl wait_cycles
    bl other_core
    b other_core_loop

core_one:
    mov r1, #0            ;@ set offset value
    movt r1, #0x4
    add r1, r1, r2        ;@ set stack offset
    mov sp, r1            ;@ set stack pointer
    mov r1,#0
    str r1,[sp]
core_one_loop:
    movw r0, #0xC6C0
    movt r0, #0x2D
    bl wait_cycles
    bl other_core
    b core_one_loop

core_two:
    mov r1, #0            ;@ set offset value
    movt r1, #0x8
    add r1, r1, r2        ;@ set stack offset
    mov sp, r1            ;@ set stack pointer
    mov r1,#0
    str r1,[sp]
    movw r0, #0x4240
    movt r0, #0xF
    bl wait_cycles
core_two_loop:
    movw r0, #0xC6C0
    movt r0, #0x2D
    bl wait_cycles
    bl other_core
    b core_two_loop

core_three:
    mov r1, #0            ;@ set stack value
    movt r1, #0xC
    add r1, r1, r2        ;@ set stack offset
    mov sp, r1            ;@ set stack pointer
    mov r1,#0
    str r1,[sp]
    movw r0, #0x8480
    movt r0, #0x1E
    bl wait_cycles
core_three_loop:
    movw r0, #0xC6C0
    movt r0, #0x2D
    bl wait_cycles
    bl other_core
    b core_three_loop

hopper:
    bx r0
