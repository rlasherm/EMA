/**
* @Author: kevin
* @Date:   30-09-2016
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 06-10-2016
* @License: GPL
*/

/* This code contains some useful functions to create noise on cores */

#include "noise.h"
#include "arm.h"
#include "uart.h"

/* This function compute a random number according to xorshift algorithm from George Marsaglia w,x,y,z should be initialized to a non-zero value
   w contain the random number generated
 */

unsigned int xorshift ( void ){
  static unsigned int seed=2463534242;
  unsigned int c = 17;
  unsigned int b = 19;
  unsigned int a = 7;

  seed^=(seed<<c);
  seed^=(seed>>b);
  seed^=(seed<<a);

  return seed;
}

/* This function compute a pgcd between two numbers
   a : integer
   b : integer 

   return pgcd value
*/
unsigned int pgcd (unsigned int a, unsigned int b){
  unsigned int r;
  while(b!=0){
    r = a%b;
    a = b;
    b = r;
  }
  return a;
}

/* dummy function to wait a certain amount of time
   a : integer, time to wait

   return 0
 */
unsigned int wait_cycles (unsigned int a){
  while(a!=0){
    a--;
  }
  return a;
}

