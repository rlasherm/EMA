/**
* @Author: kevin
* @Date:   21-10-2016
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 16-11-2016
* @License: GPL
*/

#include <stdio.h>
#include "kernel.h"
#include "arm.h"
#include "noise.h"
#include "uart.h"
#include "core.h"

/* Send a function to a specific core to execute it and go back to sleep

   core : in range CORE{1-4}
   function: is a name of a function with a return

   No return
*/
void start_other_core ( unsigned int core, void (*function)(void) ){
  put32(core, (unsigned int) function);
  put32(core, (unsigned int) dummy);
}

/* A generic function for each cores
   Return void
*/
void other_core ( void ){
  unsigned int core_id, num_core,ra,rb,rc ;

  core_id = getMPIDR();
  // uart_printhex(xorshift());
  // uart_printhex(core_id);
  // uart_printhex(num_core);

  switch(core_id){
   case CORE1:
   // start_other_core (CORE2, other_core);
     put32(MBXSETC30, xorshift()); // write result of the xorshift to MBXC30
   break;
   case CORE2:
   // start_other_core (CORE1, other_core);
     put32(MBXSETC31, xorshift());
     break;
   case CORE3:
     //     wait_cycles(1000000);
     ra = get32(MBXRDCLRC30);
     rb = get32(MBXRDCLRC31);
     rc = pgcd(ra,rb);
     put32(MBXSETC00, ra);
     put32(MBXSETC01, rb);
     put32(MBXSETC02, rc);
   // start_other_core (CORE0, other_core);
     break;
   default:
     break;
  }
  while(1){
    dummy();
  }
  //error_code_LED(num_core);
  return;
}

/* Read mailboxes for a specified core */
void getMailboxes( unsigned int core ){
  int i;
  
  core = (core << 30);
  core = core >> 30;
  uart_send(0x20);
    for ( i = 0 ; i < 4 ; i++ ){
      uart_send(0x7c);
      uart_printhex(get32((COREMBXRDCLR + (core * 16) + (i * 4))));
      uart_send(0x7c);
    }
    uart_send(0x20);
  return;
}
