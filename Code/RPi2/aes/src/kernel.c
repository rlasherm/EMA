/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 16-11-2016
* @License: GPL
*/


#include "types.h"
#include "kernel.h"
#include "arm.h"
#include "gpio.h"
#include "uart.h"
#include "timer.h"
#include "jtag.h"
#include "aes.h"
#include "noise.h"
#include "core.h"
#include <stdlib.h>
#include <stdio.h>

void gpio_init() {
  //set LED as output
  set_pin_function(LED_PIN, FSEL_OUTPUT);

  //set trig pin as output
  set_pin_function(TRIG_PIN, FSEL_OUTPUT);
}

int kernel_preinit ( unsigned int r0, unsigned int r1, unsigned int r2 )
{
  initiate_jtag();
  timer_init();
  uart_init();

  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int*  bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  //start caches
  start_l1cache();
  start_vfp();

  // start other cores (initialize stack vectors)
  // start_other_core (MBXSETC13, core_wrapper);
  // start_other_core (MBXSETC23, core_wrapper);
  // start_other_core (MBXSETC33, core_wrapper);

  //call main after preinit
  kernel_main(r0, r1, r2);

  return 0;
}

/**************************************************/
/*                  Main                          */
/**************************************************/

int kernel_main ( unsigned int r0, unsigned int r1, unsigned int r2 )
{
  //gpio_init();
  // uart_printhex(getMPIDR());
  //uart_printhex(42);
  // getMailboxes();
  //  uart_get_reg();

  unsigned char test_plain[DATA_SIZE] = {0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34};
  unsigned char test_key[DATA_SIZE] = {0x2b, 0x7e, 0X15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
  unsigned char test_cipher[DATA_SIZE] = {0x39, 0x25, 0x84, 0x1d, 0x02, 0xdc, 0x09, 0xfb, 0xdc, 0x11, 0x85, 0x97, 0x19, 0x6a, 0x0b, 0x32};

  unsigned char plaintext[DATA_SIZE];
  unsigned char ciphertext[DATA_SIZE];
  unsigned char key[DATA_SIZE];

  printf("\nShall we play a game?>\n");

  while(true) {
    //wait for command char
    unsigned char cmd = uart_recv();

    switch(cmd) {
    case 't'://test connection
      printf("Comms OK\n");
      unsigned int i;
      for(i = 0; i < DATA_SIZE; i++) {
      	plaintext[i] = test_plain[i];
      	key[i] = test_key[i];
      }
      AESEncrypt(ciphertext, plaintext, key);
      bool test = true;
      for(i = 0; i < DATA_SIZE; i++) {
        if(ciphertext[i]!=test_cipher[i]) {
          test = false;
        }
      }
      if(test) {
        printf("AES test OK\n");
      } else {
      	printf("AES test FAIL:\n");
      	printf("Plaintext: ");
      	uart_print_hexvector(plaintext, DATA_SIZE);
      	printf("\nKey: ");
      	uart_print_hexvector(key, DATA_SIZE);
      	printf("\nCiphertext: ");
      	uart_print_hexvector(ciphertext, DATA_SIZE);
      	printf("\n");
      }
      break;
    case 'k':
      uart_read_vector(key, DATA_SIZE);
      break;
    case 'p':
      uart_read_vector(plaintext, DATA_SIZE);
      break;
    case 'c':
      uart_send_vector(ciphertext, DATA_SIZE);
      break;
    case 'g':
      AESEncrypt(ciphertext, plaintext, key);
      break;
    case 'f':
      uart_read_vector(plaintext, DATA_SIZE);
      AESEncrypt(ciphertext, plaintext, key);
      uart_send_vector(ciphertext, DATA_SIZE);
        break;
    case ',':
      start_other_core (CORE1, core_wrapper);
      break;
    case ';':
      start_other_core (CORE2, core_wrapper);
      break;
    case ':':
      start_other_core (CORE3, core_wrapper);
      break;
    case 'w':
      // uart_printhex(getMPIDR());
      getMailboxes(CORE0);
      break;
    default:
      printf("Unknown command: %c\n", cmd);
    }
  }

  return 0;
}
