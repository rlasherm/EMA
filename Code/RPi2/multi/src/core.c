/**
* @Author: kevin
* @Date:   21-10-2016
* @Email:  sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 02-12-2016
* @License: GPL
*/

#include <stdio.h>
#include "kernel.h"
#include "arm.h"
#include "noise.h"
#include "uart.h"
#include "core.h"

/* Send a function to a specific core to execute it and go back to sleep

   core : in range CORE{1-4}
   function: is a name of a function with a return

   No return
*/
void start_other_core ( unsigned int core, void (*function)(void) ){
  put32(core, (unsigned int) function);
  // put32(core, (unsigned int) dummy);
}

/* A generic function for each cores
   Return void
*/
void other_core ( void ){
  unsigned int core_id, num_core,ra,rb,rc ;

  core_id = getMPIDR();
  ra = xorshift();
  rb = xorshift();
  rc = pgcd(ra,rb);

  switch(core_id){
   case CORE1:
       put32(MBXRDCLRC10,0xFFFFFFFF);
       put32(MBXRDCLRC11,0xFFFFFFFF);
       put32(MBXRDCLRC12,0xFFFFFFFF);
       put32(MBXSETC10, ra); // write result of the xorshift to MBXC10
       put32(MBXSETC11, rb); // write result of the xorshift to MBXC11
       put32(MBXSETC12, rc); // write result of the xorshift to MBXC12
   break;
   case CORE2:
       put32(MBXRDCLRC20,0xFFFFFFFF);
       put32(MBXRDCLRC21,0xFFFFFFFF);
       put32(MBXRDCLRC22,0xFFFFFFFF);
       put32(MBXSETC20, ra); // write result of the xorshift to MBXC20
       put32(MBXSETC21, rb); // write result of the xorshift to MBXC21
       put32(MBXSETC22, rc); // write result of the xorshift to MBXC22
     break;
   case CORE3:
       put32(MBXRDCLRC30,0xFFFFFFFF);
       put32(MBXRDCLRC31,0xFFFFFFFF);
       put32(MBXRDCLRC32,0xFFFFFFFF);
       put32(MBXSETC30, ra); // write result of the xorshift to MBXC30
       put32(MBXSETC31, rb); // write result of the xorshift to MBXC31
       put32(MBXSETC32, rc); // write result of the xorshift to MBXC32
     break;
   default:
     break;
  }
  return;
}

/* Read mailboxes for a specified core */
int* getMailboxes( unsigned int core ){
  int i;
  static int mailboxes[4];

  core = core & 0x3;
  //  uart_send(0x20);
    for ( i = 0 ; i < 5 ; i++ ){
      //      uart_send(0x7c);
      *(mailboxes+i) = get32((COREMBXRDCLR + (core * 16) + (i * 4)));
      //uart_send(0x7c);
    }
    //uart_send(0x20);
  return mailboxes;
}

typedef void (*func)();
/* Wait till adress given in Mailbox */
void wfi_func( unsigned int core ){
  unsigned int core_nb = core & 0x3;
  unsigned int exec_mbx;
  func exec_func;

  exec_mbx = get32(COREMBXRDCLR + (core_nb*16) + 12);

  if(exec_mbx){
    put32((COREMBXSET + ((core_nb-1)*4)),exec_mbx);
    put32((COREMBXRDCLR + (core_nb*16) + 12),0xFFFFFFFF);
    exec_func = (func)exec_mbx;
    exec_func();
  }
  return;
}

void test_dis_smp (void){
  unsigned int core_id = getMPIDR();
  switch(core_id){
  case CORE1:
     put32(MBXSETC00, core_id&0x3); // write result of the xorshift to MBXC30
     put32(MBXSETC10, getACTLR());
     put32(MBXSETC11, (int)getSP());
     put32(MBXSETC12, getCPSR());
   break;
   case CORE2:
     put32(MBXSETC01, core_id);
     put32(MBXSETC20, getACTLR());
     put32(MBXSETC21, (int)getSP());
     put32(MBXSETC22, getCPSR());
     break;
   case CORE3:
     put32(MBXSETC02, core_id);
     put32(MBXSETC30, getACTLR());
     put32(MBXSETC31, (int)getSP());
     put32(MBXSETC32, getCPSR());
     break;
   default:
     break;
  }
  return;
}

void infinite_loop_other_core( void ){
  while(1){
    other_core();
  }
  return;
}

void display_cpsr (void) {
  uart_printhex(getCPSR());
}
