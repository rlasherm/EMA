; @Author: kevin, ronan
; @Date:   17-11-2016
; @Email:  sebanjila.bukasa@inria.fr
; @Last modified by:   kevin
; @Last modified time: 17-11-2016
; @License: GPL

.section ".secureboot"

   /*
     * This is the secure boot monitor and initialization code.
     * It has to fit somewhere below 0x100 (we choose 0xEC) otherwise it may
     * get overwritten by whatever DTB the GPU bootloader sees fit to load.
     *
     *
     * NOTE: this code is not secure. In fact, the primary design objective
     *       was to be insecure.
     */

__secvectors:
    adr pc, __secreset
    adr pc, _secundefi
    adr pc, __secmon
    adr pc, _secpabort
    adr pc, _secdabort
    nop
    adr pc, _secirq
    adr pc, _secfiq

## test die ##
__die:
    wfe
    b __die


/* the unsecure secure monitor, does as being told */
__secmon:

    cmp r0, #1
    beq change_mode
    cmp r0, #2
    beq copy_mon_cpsr
    ## cmp r0, #3
    ## beq fast_change_mode

change_scr:
    /* SCR ^= r1 -> what security?? */
    mrc p15, 0, r0, c1, c1, 0
    eor r0, r0, r1
    mcr p15, 0, r0, c1, c1, 0
    movs pc, lr

change_mode:
    msr spsr_all, r1
    movs pc, lr

## copy CPSR to C0 MBX03
copy_mon_cpsr:
    mrs     r3, cpsr
    movw r8,#0x8C   
    movt r8,#0x4000
    str r3, [r8]
    movs pc,lr

## fast_change_mode:
##     cps #16
##     mov pc, #0x8000

__secreset:
    /* do not disturb, please */
    cpsid ifa

#if 0
    /* for now, allow only one CPU */
    mrc p15, 0, r1, c0, c0, 5
    ands r1, r1, #3 
    bne _start
#endif

    /* set MVBAR=_secvectors and VBAR=_start*/
    ldr r0, =__secvectors
    ## ldr r1, =__secvectors
    mcr p15, 0, r0, c12, c0, 1
    mcr p15, 0, r0, c12, c0, 0

    /* Update SCR: clear IRQ, FIQ, EA, nET. set AW, HCE, and FW  */
    mrc p15, 0, r1, c1, c1, 0
    bic r1, r1, #0x4E
    orr r1, r1, #0x130
    mcr p15, 0, r1, c1, c1, 0
    isb

    /* SCTLR: enable I and C caches */
    mrc 15, 0, r0, c1, c0, 0
    orr r0, r0, #0x0004
    orr r0, r0, #0x1000
    mcr 15, 0, r0, c1, c0, 0

    /* ACTLR: set SMP */
    mrc p15, 0, r0, c1, c0, 1
    orr r0, r0, #0x40
    mcr p15, 0, r0, c1, c0, 1

    /* CNTV_CTL enable ? */
    mov r0, #1
    mcr p15, 0, r0, c14, c3, 1

    /* NSACR = all COPs are non-sec */
    movw r1, #0x3fff
    movt r1, #6
    mcr p15, 0, r1, c1, c1, 2

    /* set CNTFRQ */
    ## ldr r0, =CNTFRQ
    ## mcr p15, 0, r0, c14, c0, 0

    ## switch to NS ##
    mov r0, #0
    mov r1, #1
    smc #0

    ## Entry point for nomal world ##
    mov pc, #0x800

_secundefi:
    b       secundefi_handler

_secpabort:
    b       secpabort_handler

_secdabort:
    b       secdabort_handler

_secirq:
    b       secirq_handler

_secfiq:
    b       secfiq_handler
