/**
* @Author: ronan
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 02-12-2016
* @License: GPL
*/


#ifndef KERNEL_H
#define KERNEL_H

#define SUMMARY  0
#define AES_MODE 1
#define PIN_MODE 2
#define LED_MODE 3
#define TT_MODE  4
#define MUL_MODE 5
#define REG_MODE 6

#define PTEXT     0
#define CTEXT     1
#define KEY       2
#define PTEXT_S  10
#define CTEXT_S  11
#define KEY_S    12
#define ENABLE    1
#define DISABLE   0
#define MAX_PIN  10
#define MASTER    1
#define CANDIDATE 2
#define LENGTH    3
#define FAST      4

extern unsigned int __bss_start__;
extern unsigned int __bss_end__;

int kernel_main ( unsigned int r0, unsigned int r1, unsigned int r2 );
int kernel_preinit ( unsigned int r0, unsigned int r1, unsigned int r2 );
void gpio_init( void );
#endif
