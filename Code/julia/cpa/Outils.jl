# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   19-10-2016
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc

#--------------------------------------------------------
# Function HW
# computes the HW of a vectors or a matrice
#--------------------------------------------------------
function HW(V)
	hw=map(v -> count_ones(v), V)
	return hw

end
#--------------------------------------------------------


#--------------------------------------------------------
# function : bit_i
# give the i th bit of a number X
# X a number
# b the bit
#--------------------------------------------------------
function bit_b(X,b)

	P=bits(X)

	p=P[end:-1:end-8]
 	
 	if(p[b]=='1')
 		pb=1
 	else
 		pb=0
 	end

  	return pb
end
#----------------------------------------------------------



#--------------------------------------------------------
# function : bit_i
# give the i th bit of elements of a vector V
# b the bit
#--------------------------------------------------------
function vector_bit_b(V,b)

	bits=map(v -> bit_b(v,b), V)
  	return bits
end
#----------------------------------------------------------



#--------------------------------------------------------
# fonction : histogramm
# X vector
# build the histogramm of values X
#--------------------------------------------------------
function histogramm(X)
	n = size(X,1)*size(X,2)
	hist = Dict()
	length(hist)

	for i=1:n
		x = X[i]
	#	#add t to dictionary
		val = get(hist, x, 0)
		hist[x] = val+1
	end

	
	return hist
end








#--------------------------------------------------------
# 
#--------------------------------------------------------

function to_text(hex)
	round(Int64,hex2bytes(hex)')
end

function to_hex(arr)
	reduce(string,map(x->hex(x,2),arr))
end
