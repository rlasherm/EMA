# @Author: Hélène Le Bouder, Ronan Lashermes
# @Date:   2016-2017
# @Email:  helene.le-bouder@inria.fr, ronan.lashermes@inria.fr
# @License: CC-by-nc


# Prediction are given for the AES.
# The mathematical model is the Hamming weight

include("AES.jl")
include("Outils.jl")

#--------------------------------------------------------
#FIRST ROUND
#--------------------------------------------------------

#--------------------------------------------------------
# function : Predictions_first_round_byte
# give predictions on the first round of AES on one byte for n texts
# T matrix of n texts (n*16)
# o number of the targeted byte
#--------------------------------------------------------
function Predictions_first_round_byte(Texts,o)

	n=size(Texts,1)
 # 	P=round.(Int64,zeros(n,256))
	#
	# I=round.(Int64,1:n)
	# map(i -> P[i,:]=Prediction_first_round_byte(Texts[i,o]), I)

	P = hcat(map(t -> Prediction_first_round_byte(t), Texts[:,o])...)'


	return P
end
#----------------------------------------------------------



#--------------------------------------------------------
# function : Predictions_first_round_byte
# give predictions on the first round of AES on one byte for n texts
# T matrix of n texts (n*16)
# o number of the targeted byte
# b number of targeted bit
#--------------------------------------------------------
function Predictions_first_round_bit(Texts,o,b)

	n=size(Texts,1)
 	P=round.(Int64,zeros(n,256))

	I=round.(Int64,1:n)
	map(i -> P[i,:]=Prediction_first_round_bit(Texts[i,o],b), I)


	return P
end
#----------------------------------------------------------


#--------------------------------------------------------
# function : Prediction_first_round_byte
# give predictions on the first round of AES on one Text_byte
#--------------------------------------------------------
function Prediction_first_round_byte(text_byte)
	K=round.(Int64,0:255)
  	Pk=map(k ->gen_prediction_first_round_byte(text_byte, k), K)
  	# Pk=HW(Pk)
	return Pk
end
#----------------------------------------------------------


#--------------------------------------------------------
# function : Prediction_first_round_bit
# give predictions on the first round of AES on one bit of a Text_byte
# b number of the targeted bit
#--------------------------------------------------------
function Prediction_first_round_bit(text_byte,b)

  	K=round.(Int64,0:255)
  	Pk=map(k ->gen_prediction_first_round_byte(text_byte, k), K)
  	Pk=vector_bit_b(Pk,b)

	return Pk
end
#----------------------------------------------------------


#--------------------------------------------------------
# function : gen_prediction_first_round_byte
# give prediction on the first round of AES on one byte
# byte : text byte
# key byte
#--------------------------------------------------------
function gen_prediction_first_round_byte(text_byte, key_byte)
	p=sbox(xor(text_byte,key_byte))
	# p=text_byte $ key_byte
  	return p
end
#----------------------------------------------------------




#--------------------------------------------------------
#LAST ROUND
#--------------------------------------------------------



#--------------------------------------------------------
# function : Predictions_last_round_byte
# give predictions on the last round of AES on one byte for n Cipher_texts
# T matrix of n Cipher_texts (n*16)
# o number of the targeted byte
#--------------------------------------------------------
function Predictions_last_round_byte(Cipher_texts,o)

	n=size(Cipher_texts,1)
 	P=round.(Int64,zeros(n,256))

	I=round.(Int64,1:n)
	map(i -> P[i,:]=Prediction_last_round_byte(Cipher_texts[i,o]), I)


	return P
end
#----------------------------------------------------------



#--------------------------------------------------------
# function : Predictions_last_round_byte
# give predictions on the last round of AES on one byte for n Cipher_texts
# T matrix of n Cipher_texts (n*16)
# o number of the targeted byte
# b number of targeted bit
#--------------------------------------------------------
function Predictions_last_round_bit(Cipher_texts,o,b)

	n=size(Cipher_texts,1)
 	P=round.(Int64,zeros(n,256))

	I=round.(Int64,1:n)
	map(i -> P[i,:]=Prediction_last_round_bit(Cipher_texts[i,o],b), I)


	return P
end
#----------------------------------------------------------


#--------------------------------------------------------
# function : Prediction_last_round_byte
# give predictions on the last round of AES on one Text_byte
#--------------------------------------------------------
function Prediction_last_round_byte(text_byte)
	K=round.(Int64,0:255)
  	Pk=map(k ->gen_prediction_last_round_byte(text_byte, k), K)
  	Pk=HW(Pk)
	return Pk
end
#----------------------------------------------------------


#--------------------------------------------------------
# function : Prediction_last_round_bit
# give predictions on the last round of AES on one bit of a Text_byte
# b number of the targeted bit
#--------------------------------------------------------
function Prediction_last_round_bit(text_byte,b)

  	K=round.(Int64,0:255)
  	Pk=map(k ->gen_prediction_last_round_byte(text_byte, k), K)
  	Pk=vector_bit_b(Pk,b)

	return Pk
end
#----------------------------------------------------------


#--------------------------------------------------------
# function : gen_prediction_last_round_byte
# give prediction on the last round of AES on one byte
# byte : text byte
# key byte
#--------------------------------------------------------
function gen_prediction_last_round_byte(text_byte, key_byte)
	p=invsbox(xor(text_byte,key_byte))
  	return p
end
#----------------------------------------------------------
