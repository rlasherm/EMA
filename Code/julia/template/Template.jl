
include("Template_bibliotheque.jl")
#--------------------------------------------------------------
# programme d'Hélène Le Bouder et Ronan Lashermes
#--------------------------------------------------------------

# k un secret ou un guess
# Fk sont des résultats de template
# Sk matrice de covarience
# mk moyenne
# ISk inverse de Sk
# Ak  cholesky de Sk

#--------------------------------------------------------------
#  fonction qui compare un vecteur aux différents templates
#  v vecteurs traces du circuit attaqué, vecteurs lignes
#  K vecteur des hypothèse (pour un code pin 0 1 2 3 4 5 6 7 8 9)
#  suppose que les templates sont déja contruits
#--------------------------------------------------------------
function Template_attack_vec(Vec,K)

	n=size(Vec,1) # n nombre de traces
	p=size(Vec,2) # p nombre de points
	N=size(K,2)   # N nombre de guesses


	# attack octet par octet
	F=zeros(N,n)

	#constante
	constante=p*log(2*pi)/2

		for i=1:N
			k=K[i]
			println("k=",k)

			mk=load_binary_matrix_f64("m$(k).binf64")

			A=load_binary_matrix_f64("A$(k).binf64")
			A=A/2
			ISk=load_binary_matrix_f64("IS$(k).binf64")

			for j=1:n
				v=Vec[j,:]
				V=v-mk

				B= V*ISk*V'

				B=B/2


				R=-(A+B+constante)
				F[i,j]=R[1]
				gc()
				gc()
			end
			mk=0
			Sk=0
			gc()
			gc()
		end
	return F

end





#--------------------------------------------------------------
#  fonction qui compare un vecteur aux différents templates
#  K vecteur des hypothèse (pour un code pin 0 1 2 3 4 5 6 7 8 9)
#  T vecteurs des hypothèses de traces obtenues
#  suppose que les templates sont déja contruits
#--------------------------------------------------------------
function Template_attack_chemin(cheminTemplate,cheminAttaque,K,T)

	taille_attaque=size(T,2)  # nombre de fichier à attaquer
	N=size(K,2)               # nombre de guesses


	Fdefine=0
	constante = 0
	n = 0
	F = 0
	p = 0

	for i1=1: N
		# chargement du Template
		k1=K[i1]
		println("Premier template : ",k1)
		mk=load_binary_matrix_f64(string(cheminTemplate,"m$(k1).binf64"))
		mk=mk[1,:]
		A=load_binary_matrix_f64(string(cheminTemplate,"A$(k1).binf64"))
		A=A/2
		ISk=load_binary_matrix_f64(string(cheminTemplate,"IS$(k1).binf64"))


		for i2=1:taille_attaque
			#chargement du fichier
			k2=T[i2]
			println("Fichier : ",k2)
			fichier=string(cheminAttaque,"attaque$(k2).bin")
			Vec=load_binary_matrix_i8(fichier)

			if Fdefine==0
				n=size(Vec,1) # n nombre de traces
				p=size(Vec,2) # p nombre de points
				#constante
				constante=p*log(2*pi)/2
				F=zeros(taille_attaque,N,n)
				Fdefine=1
			end


			for i3=1: n
				# ouverture des traces
				print("\r$(i3)")
				v=Vec[i3,:]
				V=v-mk
				#formule V*ISk*V'   avec ligne, mat, col
				B=V'*symv('U',ISk,V)
				B=B/2
				R=-(A+B+constante)
				F[i2,i1,i3]=R[1]
				B=0
				gc()
				gc()
			end
			print("\r")
		end
		mk=0
		ISk=0
		gc()
		gc()

	end

	for i=1:taille_attaque
		k=T[i]
		Fk=reshape(F[i,:,:],N,n)
		save_binary_matrix(string(cheminAttaque,"F$(k).binf64"),Fk)
	end

	return F

end



#--------------------------------------------------------------
# fonction calcul les Template
#  K vecteur des hypothèse (pour un code pin 0 1 2 3 4 5 6 7 8 9)
# chemin ou sont stockées les courbes
#--------------------------------------------------------------
function Template_construction(chemin,K)
	N=size(K,2)
	for i=1:N
		k=K[i]
		fichier=string(chemin,"template$(k).bin")
		(Sk,mk)=Covariance_opti(fichier)
		save_binary_matrix(string(chemin,"m$(k).binf64"), mk)
		Ak=CholeskyMethode(Sk)
		save_binary_matrix(string(chemin,"A$(k).binf64"),Ak)
		ISk=inv(Sk)
		save_binary_matrix(string(chemin,"IS$(k).binf64"),ISk)
		mk=0
		Sk=0
		ISk=0
		gc()
		gc()
	end

	return 0
end

#--------------------------------------------------------------
# fonction retourne les résultats du template
# K vecteur des hypothèse (pour un code pin 0 1 2 3 4 5 6 7 8 9)
#--------------------------------------------------------------
function res_template_compte(chemin,K)
	N=size(K,2)
	res=zeros(N,N)
	for i=1:N
		k=K[i]
		F=load_binary_matrix_f64(string(chemin,"F$(k).binf64"))
		n=size(F,2)
		maxi=Maxi(F')-1
		#TODO convertir to K de manière générale
		for j=1:N
			k2=K[j]
			res[i,j]=count(x->x==k2, maxi)
		end
	end

	#normalisation en pourcentage
	k=K[1]
	F=load_binary_matrix_f64(string(chemin,"F$(k).binf64"))
	n=size(F,2)
	res_norm=res*100/n

	#taux de succès final
	Success_rate=sum(diag(res_norm))/N

	return (res,res_norm,Success_rate)

end


#--------------------------------------------------------------
# fonction qui fait toute l'attaque dés le début de la construction du template à l'analyse
# chemin : là où les courbes sont stockées
# K vecteurs des hypothèses
# Chaque boucle est détaillée dans l'ordre de l'attaque
# b moyennage sur combien de courbes
#--------------------------------------------------------------
function Attaque_template_complete(cheminTemplate,K,T,b)

	Template_construction(cheminTemplate,K)
	cheminAttaque=cheminTemplate
	F=Template_attack_chemin(cheminTemplate,cheminAttaque,K,T)

	if b==0
		(res,res_norm,Success_rate)=res_template_compte(cheminAttaque,K)
	else
		cheminMoyennee=Moyennage_F(cheminAttaque,b,T)
		(res,res_norm,Success_rate)=res_template_compte(cheminMoyennee,K)
	end

	return (res,res_norm,Success_rate)
end
