using MultivariateStats

#--------------------------------------------------------------
# programme d'Hélène Le Bouder et Ronan Lashermes
#--------------------------------------------------------------

# k un secret ou un guess
# Fk sont des résultats de template
# Sk matrice de covarience
# mk moyenne
# ISk inverse de Sk
# Ak  cholesky de Sk 
# p nombre de points
# phi nombres de points après traitement

#--------------------------------------------------------------
# fonction qui compare calcul phi sous-espaces propres de Sk
# càd les vecteurs propres qui maximise la variance
# M matrice de mesure ( Attention dans M le secret doit varier !) 
# phi nombres de sous_espaces sélectionnés
#--------------------------------------------------------------

function Sous_espace_construction(M,phi)
	Trans = fit(PCA, M, maxoutdim=phi)
	return Trans
end 

#--------------------------------------------------------------
# fonction qui transforme un vec dans la nouvelle base
#--------------------------------------------------------------

function Sous_espace_transformation(V,Trans)
	V2 = transform(Trans,V)
	return V2
end 

#--------------------------------------------------------------
# fonction qui construit un nouveau jeu de courbes
# ou l'on fait varier les valeurs de secret
# K vecteurs de guess possible
# On utilise que 10 000 traces
# chemin là où sont stockées les courbes.
#--------------------------------------------------------------
function Nouveau_jeu(chemin,K,n2)
	fichier=string(chemin_base,"template0.bin")
	Vec=load_lignes_bini8(fichier,1)
	
	p=size(Vec,2)
	M=round(Int8,zeros(n2,p))
	#R=round(Int,floor((rand(n2)*10)))
	#L=round(Int,floor((rand(n2)*n)))
	
	
	N=size(K,2)
	L=round(Int,n2/10)
	for i=1:N	
		k=K[i]
		fichier=string(chemin_base,"template$(k).bin")
		M[(i-1)*L+1:((i-1)*L)+L,:]=load_lignes_bini8(fichier,L)
		
	end

	return M
end 




#-------------------------------------------------------------------------------------------------------
# Fonction qui lit les Lièmes premières lignes d'un fichier binaire binf64
#------------------------------------------------------------------------------------------------------------------------
function load_lignes_bini8(filepath,L)
	# ouverture du fichier
	Vec=0
	f = open(filepath, "r")

	if(eof(f) == true)
		close(f)
	return 0
	end
	
	#lit le nombre de lignes
	trace_len::Int32 = read(f, Int32)
	#lit le nombre de colonne
	trace_count::Int32 = read(f, Int32)
	
	
	M=round(Int8,zeros(L,trace_len))
	if(L>trace_count)
		print("erreur il n'y a pas L lignes")
		return 0
	end

  	for i=1:L
		M[i,:]= read(f, Int8, trace_len)
  	end

	close(f)
	M=round(Int8,M)
	
	return M

end



#--------------------------------------------------------------
# fonction qui construit un nouveau jeu de courbes
# K vecteurs de guess possible
# n nombres de traces
# chemin_base là où sont stockées les courbes
# phi nombre de points après traitement
# n2 nombre d'éxécution prises pour calculer la pca
#--------------------------------------------------------------
function Nouvelle_courbes(chemin_base,phi,K,n2)
	chemin_new=string(chemin_base,"Opti_pca_$(phi)")
	mkdir(chemin_new)
	
	M=Nouveau_jeu(chemin_base,K,n2)	
	M=M+0.0 #pour avoir un float
	Trans=Sous_espace_construction(M',phi)
	dim=outdim(Trans)
	M=0
	gc()
	gc()
	
	N=size(K,2)
	for i=1:N	
		k=K[i]	
		chemin_in=string(chemin_base,"template$(k).bin")
		chemin_out=string(chemin_new,"/template$(k).bin")
		Transformation(chemin_in,chemin_out,Trans,dim)
		
		
		chemin_in=string(chemin_base,"attaque$(k).bin")
		chemin_out=string(chemin_new,"/attaque$(k).bin")
		Transformation(chemin_in,chemin_out,Trans,dim)
	end
	return (dim,chemin_new)
end

	
#--------------------------------------------------------------
# fonction qui transforme un fichier de courbes
# chemin_in là où sont stockées les courbes de départ
# chemin_in là où sont stockées les nouvelles courbes
# Trans la transformation
# phi nombre de points après traitement
#--------------------------------------------------------------
function Transformation(chemin_in,chemin_out,Trans,dim)

		#transformation des courbes templates		
		fichier_in = open(chemin_in, "r")
		fichier_out = open(chemin_out, "w")
		
		if(eof(fichier_in) == true)
			close(fichier_in)
			print("Fichier vide !\n")
		end
		#nb colonnes
		trace_len= read(fichier_in, Int32)
		#nb lign
		trace_count =read(fichier_in, Int32)
		
		#nb colonn
		write(fichier_out,round(Int32, dim))
		#nb lignes
  		write(fichier_out,trace_len)
  		
		i = 1
	  	while eof(fichier_in) == false && i <= trace_count
	  		#lecture du vecteur original
			Vec=read(fichier_in, Int8, trace_len)
			#transformation dans la nouvelle base			
			Vec2=Sous_espace_transformation(float(Vec),Trans)
			
			for j=1:dim
				a=Vec2[j]
				write(fichier_out,a)
			end
	   	 	i+=1
	  	end
	  	
		close(fichier_in)	
		close(fichier_out)
		
		return 0
	
end 









