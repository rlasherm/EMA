#--------------------------------------------------------
# fonction qui fait les cpa sur tous les bits aes sbox 1er tour
# n =nombre de texts
# p =nb points
#--------------------------------------------------------

function CPA_AES_complete(n,M,T)
p=size(M,2)
n=size(M,1)
include("../cpa/Prediction_AES.jl")
include("CPA.jl")
	for o=1:16
		o
		P=Prediction_bit_AES(n,o,T)

		for b=1:8
			b
			Pb=P[:,:,b]
			cpa=CPA(M,Pb,p)
			writecsv("cpaO$(o)b$(b).csv",cpa)
			plot(cpa')
			ylabel("correlation")
			xlabel("temps")
			savefig("cpaO$(o)b$(b).pdf")
	
		end

	end

end
