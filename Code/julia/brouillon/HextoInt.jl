#--------------------------------------------------------
# fonction qui passe d'un octet en hexa à un int (c une chaine de caractère)
#--------------------------------------------------------
function HextoInt(c)
	
	x=int(c[1])
	if(x>96)
		x=x-'a'+10
	else
		x=x-'0'
	end
	
	
	y=int(c[2])
	if(y>96)
		y=y-'a'+10
	else
		y=y-'0'
	end
	
	
	n=int(16*x+y)
	
	return n
	
end
