function save_binary_matrix(M::Array{Int8,2}, filepath::ASCIIString)
	f = open(filepath, "w")

	len::Int32 = size(M,2)
	count::Int32 = size(M,1)

	write(f, len)
	write(f, count)

	for i=1:count
		for j=1:len
			write(f,M[i,j])
		end
	end

	close(f)
end
