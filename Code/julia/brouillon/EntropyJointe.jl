#--------------------------------------------------------
# fonction plus rapide qui calcule l'entropie jointe de 2 vecteurs
# V1, V2 vecteurs
# méthode par histogramme (par dictionnaire)
#--------------------------------------------------------


function EntropyJointe(V1,V2)
	n1 = size(V1,1)
	n2 = size(V2,1)
	
	dico = Dict()
	
	if (n1!=n2)
    		error("on veut des vecteurs de meme taille");
	end
	
	
	#hist
	for i=1:n1
		t = (V1[i],V2[i])
		#ajoute t au dictionnaire
		val = get(dico, t, 0)
		dico[t] = val+1
	end
	
	entropy = 0.0
	
	#calcule entropie à partir du compteur
	for k in keys(dico)
		p = dico[k] / n1
		entropy = entropy + p*log2(p)
	end
	
	return -entropy
end
