#--------------------------------------------------------
# fonction plus rapide qui calcule l'entropie d'un vecteur
# V vecteur en question
# méthode par histogramme (utlise des dictionnaires)
#--------------------------------------------------------

function Entropy(V1)
	n = size(V1,1)
	
	dico = Dict()

	
	#histogramme 
	for i=1:n
		t = (V1[i])
		#ajoute t au dictionnaire
		val = get(dico, t, 0)
		dico[t] = val+1
	end
	
	entropy = 0.0
	
	#calcule entropie à partir du compteur
	for k in keys(dico)
		p = dico[k] / n
		entropy = entropy + p*log2(p)
	end
	
	return -entropy
end
