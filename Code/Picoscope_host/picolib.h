#ifndef picolib_h
#define picolib_h

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>

#include "libps3000a-1.1/ps3000aApi.h"
#include "libps3000a-1.1/PicoStatus.h"

#define PREF4 __stdcall

#define BUFFER_SIZE 	1024

#define QUAD_SCOPE		4
#define DUAL_SCOPE		2

/*
typedef int bool;
#define TRUE 1
#define FALSE 0

*/

typedef int bool;
enum { FALSE, TRUE };

typedef enum
{
	ANALOGUE,
	DIGITAL,
	AGGREGATED,
	MIXED
}MODE;


typedef struct
{
	short DCcoupled;
	short range;
	short enabled;
}CHANNEL_SETTINGS;

typedef enum
{
	MODEL_NONE = 0,
	MODEL_PS3204A	= 0xA204,
	MODEL_PS3204B	= 0xB204,
	MODEL_PS3205A	= 0xA205,
	MODEL_PS3205B	= 0xB205,
	MODEL_PS3206A	= 0xA206,
	MODEL_PS3206B	= 0xB206,
	MODEL_PS3207A	= 0xA207,
	MODEL_PS3207B	= 0xB207,
	MODEL_PS3404A	= 0xA404,
	MODEL_PS3404B	= 0xB404,
	MODEL_PS3405A	= 0xA405,
	MODEL_PS3405B	= 0xB405,
	MODEL_PS3406A	= 0xA406,
	MODEL_PS3406B	= 0xB406,
	MODEL_PS3204MSO = 0xD204,
	MODEL_PS3205MSO = 0xD205,
	MODEL_PS3206MSO = 0xD206,
} MODEL_TYPE;

typedef enum
{
	SIGGEN_NONE = 0,
	SIGGEN_FUNCTGEN = 1,
	SIGGEN_AWG = 2
} SIGGEN_TYPE;

typedef struct tTriggerDirections
{
	PS3000A_THRESHOLD_DIRECTION channelA;
	PS3000A_THRESHOLD_DIRECTION channelB;
	PS3000A_THRESHOLD_DIRECTION channelC;
	PS3000A_THRESHOLD_DIRECTION channelD;
	PS3000A_THRESHOLD_DIRECTION ext;
	PS3000A_THRESHOLD_DIRECTION aux;
}TRIGGER_DIRECTIONS;

typedef struct tPwq
{
	PS3000A_PWQ_CONDITIONS_V2 * conditions;
	short nConditions;
	PS3000A_THRESHOLD_DIRECTION direction;
	unsigned long lower;
	unsigned long upper;
	PS3000A_PULSE_WIDTH_TYPE type;
}PWQ;

typedef struct
{
	short handle;
	MODEL_TYPE				model;
	PS3000A_RANGE			firstRange;
	PS3000A_RANGE			lastRange;
	short					channelCount;
	short					maxValue;
	short					sigGen;
	short					ETS;
	short					AWGFileSize;
	CHANNEL_SETTINGS		channelSettings [PS3000A_MAX_CHANNELS];
	short					digitalPorts;
}UNIT;


typedef struct tBufferInfo
{
	UNIT * unit;
	MODE mode;
	short **driverBuffers;
	short **appBuffers;
	short **driverDigBuffers;
	short **appDigBuffers;

} BUFFER_INFO;

void openPico(void);
void closePico(void);
void printError(void);
void get_info(UNIT * unit);
int adc_to_mv(long raw, int ch);
short mv_to_adc(short mv, short ch);
void DisplaySettings();

//Le trigger est déclanché au front montant quand channel dépasse mv (en mV)
void activateTrigger(short mv, PS3000A_CHANNEL channel);
void desactivateTrigger(void);

//règle la résolution horizontale
void setTimebase();//envoie la valeur à l'oscillo
void tuneSampling(unsigned long tb);


//active le channel et fixe la résolution verticale
void setChannelRange(PS3000A_CHANNEL channel, PS3000A_RANGE range);

PICO_STATUS DisableAnalogue();
void run();
void setNumberOfSamples(long samples);
bool isReady();
void readData(short* buffer, PS3000A_CHANNEL channel);
void readDataToCSV(PS3000A_CHANNEL channel, char* filename, int filename_size);

void write_Binary_Header(FILE* out, int trace_count);//Write header: (size of trace (4 bytes))
void appendDataToBinary(PS3000A_CHANNEL channel, FILE* out);
void appendDataToCSV(PS3000A_CHANNEL channel, FILE* out);

#endif
